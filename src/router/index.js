import {createRouter, createWebHistory} from 'vue-router'


const routes = [
    {
        path: '/',
        name: 'Index',
        component: () => import('../views/Index.vue'),
        meta: {
            title: '首页-墨盘',
            keepAlive: true, // 需要被缓存
        }
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('../views/Login.vue'),
        meta: {
            title: '登录-墨盘',
            keepAlive: true, // 需要被缓存
        }
    },
    {
        path: '/register',
        name: 'Register',
        component: () => import('../views/Register.vue'),
        meta: {
            title: '注册-墨盘',
            keepAlive: true, // 需要被缓存
        }
    },
    {
        path: '/share',
        name: 'Share',
        component: () => import('../views/Share.vue'),
        meta: {
            title: '分享列表-墨盘',
            keepAlive: true, // 需要被缓存
        }
    },
    {
        path:'/s/:flag',
        name:'ShareInfo',
        component: () => import('../views/ShareInfo.vue'),
        meta: {
            title: '分享-墨盘',
            keepAlive: true, // 需要被缓存
        }
    },
    {
        path:'/push',
        name:'Push',
        component:()=>import('../views/Push'),
        meta:{
            title:'推送文件-墨盘',
            keepAlive: false,
        }
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})
router.beforeEach(async (to, from, next) => {
    window.document.title = to.meta.title == undefined?'墨盘':to.meta.title;
    if (to.path == '/register' || to.path == '/login' || to.name == 'ShareInfo' || to.name=='Push') {
        next();
        return
    } else if(!localStorage.getItem('token')){
        next('/login')
        return
    }
    next();
});
export default router
