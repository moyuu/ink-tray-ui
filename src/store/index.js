import { createStore } from 'vuex'

export default createStore({
  state: {
    token:'',
    userInfo:{},
    uuid:'',
    sid:'',
    rid:'',
  },
  getters:{
    getToken:state=>{
      return state.token || localStorage.getItem("token") || null;
    },
    getUserInfo:state=>{
      return state.userInfo || localStorage.getItem("userInfo") || null;
    },
    getUUid:state=>{
      return state.uuid || localStorage.getItem('uuid') || null;
    },
    getSid:state=>{
      return state.sid || localStorage.getItem('sid') || null;
    },
    getIdentity:state=>{
      return {
        uuid:state.uuid || localStorage.getItem('uuid') || null,
        sid:state.sid || localStorage.getItem('sid') || null
      }
    }
  },
  mutations: {
    login(state,payload){
      state.token = payload.token;
      state.userInfo = payload.userInfo;
      localStorage.setItem('token',payload.token);
      localStorage.setItem('userInfo',payload.userInfo);
    },
    logout(state){
      state.token = '';
      state.userInfo = {};
      localStorage.removeItem('token');
      localStorage.removeItem('userInfo',payload.userInfo);
    },
    setIdentity(state,authority){
      localStorage.setItem('uuid',authority.uuid);
      localStorage.setItem('sid',authority.sid);
      state.sid = authority.sid;
      state.uuid = authority.uuid;
      if(authority.indexOf('rid') > -1){
        localStorage.setItem('rid',authority.rid);
        state.rid = authority.rid;
      }
    },
  },
  actions: {
    setIdentity(context){
      context.commit('setIdentity');
    }
  },
  modules: {
  }
})
