import './plugins/axios'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus'
import 'element-plus/lib/theme-chalk/index.css';
import axios from "@/plugins/axios";
import VideoPlayer from 'vue-video-player/src';
import 'vue-video-player/src/custom-theme.css';
import 'video.js/dist/video-js.css';
import layer from 'vue-layer';
import 'vue-layer/lib/vue-layer.css';
import Cookie from 'js-cookie';
const app = createApp(App);
app.use(store)
    .use(router)
    .use(ElementPlus)
    .use(VideoPlayer)
    .mount('#app');

app.config.globalProperties.$axios = axios;
app.config.globalProperties.$layer = layer;
app.config.globalProperties.$cookie = Cookie;
